package b137.velasco.s03a1;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args){
        System.out.println("Factorial \n");


        // Activity:
        // Create a Java Program that accepts an integer and computes for the factorial value and display it to the console.

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter integer: ");

        int num = scan.nextInt();

        long i, fact = 1;

        for(i = 1; i < num; i++ ){

            fact = fact * i;
        }

        System.out.println("factoral = "+fact);

    }

}
